import React from 'react';
import {Navbar} from 'react-materialize';
import {
    Link
} from 'react-router-dom';
const HeaderNoLogin= ({logo})=>{
    return(
        <Navbar className="navbar-main"  brand={logo} right fixed={true}>
            
        </Navbar>
    )
}
export default HeaderNoLogin;
