import React, { Component } from 'react';
import cookie from 'js-cookie';
import axios from 'axios';
import HeaderNoLogin from './HeaderNoLogin';
import HeaderLogin from './HeaderLogin';
import Logo from '../../img/logo_fesa.png';
const logo = <img src={Logo} className="responsive-img" style={{height:55,paddingTop:10, paddingLeft:20}} alt='FES Acatlán'/>
class Header extends Component{
    constructor(props){
        super(props);
        this.logOut=this.logOut.bind(this);
    }
    logOut= async () => {
        await axios.post('/impresiones/logout');
        cookie.remove('account');
        window.location = '/';
    }
    render(){
        return(
            ( cookie.set('account') ? <HeaderLogin logo={logo} logOut={this.logOut}/>
        : <HeaderNoLogin logo={logo}/>)

        )
    }
}
export default Header;
