import React from 'react';
import {Navbar} from 'react-materialize';
import {
    Link
} from 'react-router-dom';

const HeaderLogin= ({logo, logOut})=>{

    return(
        <Navbar className="navbar-main"  brand={logo} right fixed={true}>
            <li>
                <a onClick={logOut}>
                    <div>Salir</div>
                </a>
            </li>
        </Navbar>
    )
}
export default HeaderLogin;
