import React, { Component } from 'react';
import {Button, Input, Row, } from 'react-materialize';
import axios from 'axios';
import cookie from 'js-cookie';
import Header from './../../components/Header';
class Login extends Component{
    constructor(props)
    {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
        this.login = this.login.bind(this);
        this.keyPress = this.keyPress.bind(this);
        this.state={
            account:'',
            nip: ''
        }
    }
    keyPress(event) {
        if (event.key === 'Enter') {
            this.login()
        }
    }
    changeHandler(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    login = () => {
        if (this.state.nip.length < 1) return;
        if (this.state.account.length < 6) return;
        const user = {
            usr: this.state.account.trim(),
            pw: this.state.nip
        }
        axios.post('/impresiones/login', user).then(res => {
            const validRegistration = res.data.valid;
            if (validRegistration) {
                cookie.set('account', user.usr, { expires: 7 });
                window.location = '/';
            } else {
                window.Materialize.toast('Numero de cuenta o nip incorrectos', 5000,'red rounded');
            }
        })
    }
    render(){
        return(
            <Row>
                <Header />
                <div className='container main-container'>
                    <h5 className='center'>Ingresar</h5>
                    <Input s={12} label="Numero de cuenta" name='account' onChange={this.changeHandler} onKeyPress={this.keyPress} value={this.state.account}></Input>
                    <Input  s={12} type="password" label="NIP" name='nip' onChange={this.changeHandler} onKeyPress={this.keyPress} value={this.state.nip}/>
                    <div className='center'>
                        <Button onClick={this.login}>Login</Button>
                    </div>
                </div>
            </Row>
        )
    }
}
export default Login;
