import React, { Component } from 'react';
import Header from './../../components/Header';
import {Icon, Table,Button } from 'react-materialize';
import axios from 'axios';
import './style.css';
class Account extends Component{
	constructor(props){
		super(props);
		this.state={
			uploadProgress:0,
			filesUpload: []
		}
		this.uploadFile=this.uploadFile.bind(this);
		this.fileChangedHandler=this.fileChangedHandler.bind(this);
	}
	componentDidMount=async ()=>{
		const getFileUser = await axios.post('/impresiones/user/getFile')
			.then((resp)=>{
				return resp.data;
			})
			.catch((err)=>{
				console.log(err);
			})
		this.setState({filesUpload : getFileUser});
	}
	fileChangedHandler = async(event) => {
    	const file = event.target.files[0]
		if(this.validateFormat(file.type))
        {
			var formData = new FormData()
	        await formData.append('file', file, file.name);
			axios.post('/impresiones/user/post/file', formData, {
                onUploadProgress: progressEvent => {
                    let progress= ((progressEvent.loaded / progressEvent.total)*100).toFixed(2);
                    this.setState({uploadProgress: progress});
					console.log(progress);
                }
            })
            .then((resp)=>{
                this.setState({filesUpload:  [...this.state.filesUpload, resp.data]})
            })
            .catch((err)=>{
                console.log(err);
            })
		}
    }
	validateFormat(format){
        const mediaType = format.split('/')[0];
        return mediaType !== 'video';
    }
	uploadFile=()=>{
		document.getElementById('fileUpload').click();
	}
	renderObject(){
		if(this.state.filesUpload.length===0) return false;
		const filesCopy = this.state.filesUpload;
		return filesCopy.map((item, index)=>{
			return(
				<tr>
					<td>{item.nameFile}</td>
					<td>{item.hojas}</td>
					<td>{item.size}</td>
					<td>{item.fechaDeCarga}</td>
				</tr>
			)
		});
	}
	render(){
		return(
			<div>
				<Header />
				<div className='container main-container'>
					<Table bordered>
						<thead>
						    <tr>
								<th data-field="id">Nombre</th>
								<th data-field="name">Numero de hojas</th>
								<th data-field="price">Tamaño archivo</th>
								<th>Fecha</th>
						    </tr>
						</thead>
						<tbody>
							{this.renderObject()}
						</tbody>
					</Table>
				</div>
				<div className='fixed-action-btn' style={{bottom: '45px', right: '24px'}}>
					<Button  onClick={this.uploadFile} floating waves='light' icon='publish' className='red' large>
					</Button>
				</div>
				<input id='fileUpload' className='hide' name='file' type="file"  s={12} onChange={this.fileChangedHandler} />
			</div>
		)
	}
}
export default Account;
