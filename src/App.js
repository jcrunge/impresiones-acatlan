import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route, Switch
} from 'react-router-dom';
import {Preloader} from 'react-materialize';
import axios from 'axios';
import Archivos from './containers/Account';
import Login from './containers/Login';
class App extends Component {
    state={
        authenticated:false,
        loading: true,
    }
    componentDidMount() {
        axios.get('/impresiones/valideSession').then(res => {
            if (res.data==='OK') {
                this.setState({
                    authenticated: true,
                    loading: false
                });
            } else {
                this.setState({authenticated: false, loading: false})
            }
        }).catch(err => {
            this.setState({authenticated: false,loading: false})
        })
    };
    render() {
        return (
            <Router>
                {this.state.loading ? <div className="center-align" style={{paddingTop:45}}><Preloader className="center-align" size='big'/></div>:
                <Switch>
                    <Route exact path="/" render={props => (this.state.authenticated) ? <Archivos /> : <Login />}/>
                </Switch>
                }
            </Router>
        );
    }
}

export default App;
